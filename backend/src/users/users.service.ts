import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities';
import { User as UserType } from '@src/common';

import { CreateUserDto } from './dtos';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(email: string): Promise<User> {
    return this.usersRepository.findOne({ email });
  }

  async checkEmail(email: string): Promise<boolean> {
    return (await this.usersRepository.findOne({ email })) ? true : false;
  }

  async createUser(createUserDto: CreateUserDto): Promise<UserType> {
    const user = this.usersRepository.create(createUserDto);
    return (await this.usersRepository.save(user)).toPublicJson();
  }
}
