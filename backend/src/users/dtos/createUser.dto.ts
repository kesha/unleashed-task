import { IsEmail, IsNotEmpty, IsString, Matches } from 'class-validator';

export class CreateUserDto {
  @Matches(new RegExp(/\s*(\S\s*){5,}/), {
    message: 'The full name should be at least 5 characters',
  })
  @IsNotEmpty({ message: 'the Full name is required' })
  fullname: string;

  @IsEmail({ message: 'The email is not valid E-mail!' })
  @IsNotEmpty({ message: 'Email is required' })
  email: string;

  @IsNotEmpty({ message: 'Password is required' })
  @Matches(new RegExp(/(?=.*\d)(?=.*[aA-zZ]).{8,}/), {
    message:
      'The password should be minimum 8 characters with at least one number and one character',
  })
  password: string;
}
