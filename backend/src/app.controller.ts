import {
  Controller,
  Get,
  UseGuards,
  Post,
  Request,
  Query,
  Body,
} from '@nestjs/common';
import { AuthService, JwtAuthGuard, LocalAuthGuard } from '@src/auth';
import { UsersService, CreateUserDto } from '@src/users';
import { User } from '@src/common';

@Controller()
export class AppController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('/auth/login')
  async login(@Request() req): Promise<{ user: User; accessToken: string }> {
    return this.authService.getToken(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @Get('/checkEmail')
  checkEmail(@Query() query) {
    const { email } = query;
    console.log({ email });
    return this.usersService.checkEmail(email);
  }

  @Post('/auth/register')
  async register(
    @Body() createUserDto: CreateUserDto,
  ): Promise<{ user: User; accessToken: string }> {
    const user = await this.usersService.createUser(createUserDto);
    return this.authService.getToken(user);
  }
}
