import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '@src/users';
import { User } from '@src/common';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<User> {
    const user = await this.usersService.findOne(email);
    if (user) {
      const isMatch = await bcrypt.compare(pass, user.password);
      if (!isMatch) return null;
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async getToken(user: User) {
    const payload = { fullname: user.fullname, sub: user.email };
    return {
      user,
      accessToken: this.jwtService.sign(payload),
    };
  }
}
