FROM node:14-alpine

RUN apk add --no-cache su-exec tini

# Prepare app dir
WORKDIR /srv/app
RUN chown node:node .

# Install npm packages
COPY package.json package-lock.json ./
ENV NODE_ENV development
RUN set -ex; \
    apk add --no-cache --virtual .gyp python3 make g++; \
    su-exec node npm ci; \
    su-exec node npm cache clean --force; \
    apk del .gyp;

# Copy source code and config
COPY --chown=node:node tsconfig.json tsconfig.build.json nest-cli.json .eslintrc.js .prettierrc ./
COPY --chown=node:node ./src ./src
# COPY --chown=node:node ./test ./test

# Build
RUN su-exec node env NODE_ENV=production npm run build

# Runtime setup
USER node
EXPOSE 8083
ENV PORT 8083
ENV NODE_ENV production
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "dist/main"]