const { alias } = require("react-app-rewire-alias");

module.exports = function override(config) {
  alias({
    "@Helpers": "src/Helpers",
    "@Services": "src/Services",
    "@Pages": "src/Pages",
    "@Components": "src/Components",
    "@Errors": "src/Errors",
    "@Hooks": "src/Hooks",
    "@Store": "src/Store",
    "@Interfaces": "src/Interfaces",
    "@Router": "src/Router",
    "@Validations": "src/Validations",
    "@Constants": "src/Constants",
    "@UICore": "src/UICore",
  })(config);

  return config;
};
