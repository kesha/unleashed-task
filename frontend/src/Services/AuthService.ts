import { LocalStorageManager } from "@Helpers";
import { LoginCredentials, User } from "@Interfaces";
import { APIService } from "./APIService";
import { LocalStorageKeys } from "@Constants";

interface AuthResponse {
  user: User;
  accessToken: string;
}

class AuthService extends APIService {
  saveResponseInLocalStorage(response: AuthResponse) {
    const { user, accessToken } = response;
    LocalStorageManager.save(LocalStorageKeys.USER, user, true);
    LocalStorageManager.save(LocalStorageKeys.AUTH, accessToken);
    return user;
  }

  async login(loginCredentials: LoginCredentials): Promise<User> {
    return this.saveResponseInLocalStorage(
      await this.post<LoginCredentials, AuthResponse>(
        "/auth/login",
        loginCredentials
      )
    );
  }

  async register(user: User): Promise<User> {
    return this.saveResponseInLocalStorage(
      await this.post<User, AuthResponse>("/auth/register", user)
    );
  }

  logout() {
    LocalStorageManager.clear();
  }

  async checkEmail(email: string): Promise<boolean> {
    return this.get<boolean>("/checkEmail", {
      params: { email },
    });
  }
}

const authService = new AuthService({
  baseURL: String(process.env.REACT_APP_API_URL),
});

Object.freeze(authService);

export default authService;
