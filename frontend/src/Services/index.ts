import authService from "./AuthService";
import profileService from "./ProfileService";
const services = { authService, profileService };
export default services;
