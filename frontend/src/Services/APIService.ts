import { LocalStorageKeys } from "./../Constants/localStorageKeys";
import { UnauthorizedError } from "./../Errors/UnauthorizedError";
import axois, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosRequestHeaders,
  AxiosResponse,
} from "axios";

import { assert, LocalStorageManager } from "@Helpers";
import { BadRequestError } from "@Errors";
import { STATUS_CODE } from "@Constants";

export interface IAPIServiceOptions {
  baseURL: string;
  needsAuth?: boolean;
  timeout?: number;
  json?: boolean;
}
const TIME_OUT = 500;

export class APIService {
  protected client: AxiosInstance;
  constructor({
    baseURL,
    timeout = TIME_OUT,
    needsAuth,
    json = true,
  }: IAPIServiceOptions) {
    const params: AxiosRequestConfig = {
      baseURL,
      timeout: timeout,
    };

    const headers: AxiosRequestHeaders = {};

    if (needsAuth) {
      headers.Authorization = `Bearer ${LocalStorageManager.get(
        LocalStorageKeys.AUTH
      )}`;
    }

    if (json) {
      headers.Accept = "application/json";
      headers["Content-Type"] = "application/json";
    }

    if (Object.keys(headers).length > 0) {
      params.headers = headers;
    }

    this.client = axois.create(params);

    const handleRequest = (request: AxiosRequestConfig): AxiosRequestConfig => {
      console.log("Request is sent");
      return request;
    };

    const handleResponse = (response: AxiosResponse): AxiosResponse => {
      console.log("Response is back");
      return response;
    };

    this.client.interceptors.request.use(handleRequest);
    this.client.interceptors.response.use(handleResponse, (error) => {
      if (error.isAxiosError) {
        const {
          response: {
            data: { message },
            status,
          },
        } = error;
        if (status === STATUS_CODE.BAD_REQUEST) {
          throw new BadRequestError(...message);
        } else if (status === STATUS_CODE.UNAUTHORIZED) {
          LocalStorageManager.clear();
          // eslint-disable-next-line no-restricted-globals
          if (location.href.indexOf("login") === -1) {
            // eslint-disable-next-line no-restricted-globals
            location.href = "/login";
          }
          throw new UnauthorizedError();
        }
      }
    });
  }

  private async request<T>(config: AxiosRequestConfig): Promise<T> {
    const headers: AxiosRequestHeaders = {};
    headers.Authorization = `Bearer ${LocalStorageManager.get(
      LocalStorageKeys.AUTH
    )}`;

    const result: AxiosResponse<T> = await this.client.request<T>({
      ...config,
      headers,
    });
    return result.data;
  }

  protected async get<T>(
    path: string,
    options: AxiosRequestConfig = {}
  ): Promise<T> {
    assert(path, "APIService.get(): Please provide a path.");

    return this.request<T>({
      method: "GET",
      url: path,
      ...options,
    });
  }

  protected async post<B, T>(
    path: string,
    payload: B,
    options: AxiosRequestConfig = {}
  ): Promise<T> {
    assert(path, "APIService.post(): Please provide a path.");

    return this.request({
      method: "POST",
      url: path,
      data: payload,
      ...options,
    });
  }
}
