import { User } from "@Interfaces";
import { APIService } from "./APIService";

class ProfileService extends APIService {
  getProfile(): Promise<User> {
    return this.get<User>("/profile");
  }
}

const profileService = new ProfileService({
  baseURL: String(process.env.REACT_APP_API_URL),
  needsAuth: true,
});

Object.freeze(profileService);

export default profileService;
