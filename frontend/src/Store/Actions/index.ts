import { User } from "@Interfaces";
import { ACTIONS_TYPES } from "@Store/Actions-Types";

export interface LoadMeAction {
  type: ACTIONS_TYPES.LOAD_ME;
  payload: User;
}

export interface LogoutAction {
  type: ACTIONS_TYPES.LOGOUT;
}

export type Action = LoadMeAction | LogoutAction;
