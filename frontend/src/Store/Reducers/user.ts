import { User } from "../../Interfaces";
import { Action } from "../Actions";
import { ACTIONS_TYPES } from "../Actions-Types";

interface UserState {
  me?: User | null;
}

export const userReducer = (
  state: UserState = { me: null },
  action: Action
) => {
  switch (action.type) {
    case ACTIONS_TYPES.LOAD_ME:
      return { ...state, me: { ...action.payload } };
    case ACTIONS_TYPES.LOGOUT:
      return { ...state, me: null };
    default:
      return state;
  }
};
