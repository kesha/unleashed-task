export const enum ACTIONS_TYPES {
  LOAD_ME = "loadme",
  LOGOUT = "logout",
}
