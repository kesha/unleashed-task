import { Dispatch } from "react";
import { User } from "@Interfaces";
import { LoadMeAction, LogoutAction } from "@Store/Actions";
import { ACTIONS_TYPES } from "@Store/Actions-Types";

export const loadMe = (user: User) => {
  return (dispath: Dispatch<LoadMeAction>) => {
    dispath({
      type: ACTIONS_TYPES.LOAD_ME,
      payload: user,
    });
  };
};

export const logout = () => {
  return (dispath: Dispatch<LogoutAction>) => {
    dispath({
      type: ACTIONS_TYPES.LOGOUT,
    });
  };
};
