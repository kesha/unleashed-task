import { FC, lazy, Suspense } from "react";
import { Route, Routes, Navigate } from "react-router-dom";

import { IRoute } from "@Interfaces";
import { LocalStorageManager } from "@Helpers";
import { LocalStorageKeys } from "@Constants";

export const ROUTES: any[] = [
  {
    path: "/main",
    key: "MAIN",
    component: lazy(() =>
      import(/*webpackChunkName: "home-page" */ "@Pages/Main").then(
        (module) => ({ default: module.HomePageComponent })
      )
    ),
    auth: true,
  },
  {
    path: "/login",
    key: "LOGIN",
    component: lazy(() =>
      import(/*webpackChunkName: "login-page" */ "@Pages/LoginRegister").then(
        (module) => ({ default: module.LoginRegisterPageComponent })
      )
    ),
    auth: false,
  },
];

export const RenderRoutes: FC<{ routes: IRoute[] }> = ({ routes }) => {
  const token = LocalStorageManager.get(LocalStorageKeys.AUTH);

  return (
    <Suspense fallback={<div>'loading...'</div>}>
      <Routes>
        {routes.map((route) => (
          <Route
            path={route.path}
            key={route.key}
            element={
              route.auth && !token ? (
                <Navigate to={"/login"} replace />
              ) : (
                <route.component />
              )
            }
          />
        ))}

        <Route path="*" element={<Navigate to={"/main"} replace />} />
      </Routes>
    </Suspense>
  );
};
