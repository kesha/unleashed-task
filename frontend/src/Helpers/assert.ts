import { AssertionError } from "../Errors";

export const assert = (
  condition: unknown,
  message: string,
  ...codes: string[]
) => {
  if (!condition) {
    throw new AssertionError(message || "Assertion failed", ...codes);
  }
};
