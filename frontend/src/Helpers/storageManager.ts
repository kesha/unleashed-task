class StorageManager {
  save(key: string, data: unknown, parse = false) {
    localStorage.setItem(key, parse ? JSON.stringify(data) : String(data));
  }

  get(key: string, parse = false) {
    try {
      const data = localStorage.getItem(key);
      if (!data) return null;
      return parse ? JSON.parse(data) : data;
    } catch (err) {
      return null;
    }
  }

  clear() {
    localStorage.clear();
  }
}

const storageManger = new StorageManager();
Object.freeze(storageManger);

export const LocalStorageManager = storageManger;
