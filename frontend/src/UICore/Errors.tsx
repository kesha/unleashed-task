import { Alert } from "antd";
import { FC } from "react";

interface ErrorsProps {
  errors: string[];
}

export const Errors: FC<ErrorsProps> = (props: ErrorsProps) => {
  const { errors = [] } = props;

  return (
    <>
      {errors.map((error, index) => (
        <Alert
          style={{ marginBottom: ".5rem" }}
          message={error}
          key={index}
          type="error"
          showIcon
          closable
        />
      ))}
    </>
  );
};
