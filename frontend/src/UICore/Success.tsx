import { Alert } from "antd";
import { FC } from "react";

interface SuccessProps {
  message: string;
}

export const Success: FC<SuccessProps> = (props: SuccessProps) => {
  const { message } = props;
  return (
    <>
      {message ? (
        <Alert
          style={{ marginBottom: ".5rem" }}
          message={message}
          type="success"
          showIcon
          closable
        />
      ) : null}
    </>
  );
};
