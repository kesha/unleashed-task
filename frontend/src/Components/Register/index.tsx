import React, { FunctionComponent, useState, memo } from "react";
import { Form, Input, Button } from "antd";
import { useNavigate } from "react-router-dom";

import { useActionCreators, useService } from "@Hooks";
import { User } from "@Interfaces";
import { BadRequestError, ValidationError } from "@Errors";
import {
  EMAIL_RULES,
  getConfirmPasswordRules,
  isEmail,
  NAME_RULES,
  PASSWORD_RULES,
} from "@Validations";
import { Errors } from "@UICore";

export const RegisterComponent: FunctionComponent<{}> = memo(() => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(false);
  const [errors, setErrors] = useState<string[]>([]);
  const [form] = Form.useForm();
  const { authService } = useService();
  const { loadMe } = useActionCreators();

  const onSubmit = (values: User) => {
    try {
      setLoading(true);
      setErrors([]);
      authService.register(values).then((user) => {
        loadMe(user);
        navigate("/main");
      });
    } catch (err) {
      if (err instanceof BadRequestError) {
        setErrors(err.codes);
      }
    } finally {
      setLoading(false);
    }
  };

  const asyncValidateEmail = async (
    _: Record<string, any>,
    email: string
  ): Promise<boolean> => {
    if (isEmail(email)) {
      const exist = await authService.checkEmail(email);
      if (exist) {
        throw new ValidationError("Email already exists");
      }
    }
    return Promise.resolve(true);
  };

  return (
    <>
      <Errors errors={errors} />
      <Form
        form={form}
        name="registerForm"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        initialValues={{ remember: false }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item label="Full Name" name="fullname" rules={NAME_RULES}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            ...EMAIL_RULES,
            {
              validator: asyncValidateEmail,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item label="Password" name="password" rules={PASSWORD_RULES}>
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Confirm Password"
          name="cpassword"
          rules={getConfirmPasswordRules()}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Button loading={loading} type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    </>
  );
});
