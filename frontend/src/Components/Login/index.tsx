import React, { FunctionComponent, useState, memo } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Input, Button } from "antd";

import { useActionCreators, useService } from "@Hooks";
import { EMAIL_RULES, PASSWORD_RULES } from "@Validations";
import { Errors } from "@UICore";
import { LoginCredentials } from "@Interfaces";
import { UnauthorizedError } from "@Errors";

export const LoginComponent: FunctionComponent<{}> = memo(() => {
  const navigate = useNavigate();
  const [errors, setErrors] = useState<string[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const { authService } = useService();
  const { loadMe } = useActionCreators();

  const onSubmit = (values: LoginCredentials) => {
    try {
      setLoading(true);
      setErrors([]);
      authService.login(values).then((user) => {
        loadMe(user);
        setLoading(false);
        navigate("/main");
      });
    } catch (err) {
      if (err instanceof UnauthorizedError) {
        setErrors(["Invalid email/password"]);
        setLoading(false);
      }
    }
  };

  return (
    <>
      <Errors errors={errors} />
      <Form
        name="loginForm"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        initialValues={{ remember: true }}
        onFinish={onSubmit}
        autoComplete="off"
      >
        <Form.Item label="email" name="email" rules={EMAIL_RULES}>
          <Input />
        </Form.Item>

        <Form.Item label="Password" name="password" rules={PASSWORD_RULES}>
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Button loading={loading} type="primary" htmlType="submit">
            Login
          </Button>
        </Form.Item>
      </Form>
    </>
  );
});
