import React from "react";
import { Button, Layout } from "antd";

import { ServiceProvider, useCurrentUser, useLogout } from "@Hooks";
import { ROUTES, RenderRoutes } from "@Router";

import "./App.css";

const { Header, Footer, Content } = Layout;

function App() {
  const user = useCurrentUser();
  const ref = useLogout();

  return (
    <Layout>
      <Header className="header">
        {user ? (
          <>
            Hello {user?.fullname}{" "}
            <Button ref={ref} type="link">
              Logout
            </Button>
          </>
        ) : (
          "Header"
        )}
      </Header>
      <Content className="content">
        <ServiceProvider>
          <RenderRoutes routes={ROUTES} />
        </ServiceProvider>
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  );
}

export default App;
