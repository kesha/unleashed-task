import { Rule } from "antd/lib/form";
const enum Types {
  EMAIL = "email",
}

const enum VALIDATION_MESSAGE {
  NAME = "The full name should be at least 5 characters",
  EMAIL = "The input is not valid E-mail!",
  PASSWORD = "The password should be minimum 8 characters with at least one number and one character",
  CONFIRM_PASSWORD = "The two passwords that you entered do not match!",
}

export const NAME_RULES: Rule[] = [
  {
    required: true,
    pattern: new RegExp(/\s*(\S\s*){5,}/),
    message: VALIDATION_MESSAGE.NAME,
  },
];

export const EMAIL_RULES: Rule[] = [
  {
    required: true,
    type: Types.EMAIL,
    message: VALIDATION_MESSAGE.EMAIL,
  },
];

export const PASSWORD_RULES: Rule[] = [
  {
    required: true,
    pattern: new RegExp(/(?=.*\d)(?=.*[aA-zZ]).{8,}/),
    message: VALIDATION_MESSAGE.PASSWORD,
  },
];

export function getConfirmPasswordRules(): Rule[] {
  return [
    {
      required: true,
      message: "Please confirm your password!",
    },
    ({ getFieldValue }: Record<string, any>) => ({
      validator(_: Record<string, any>, value: string) {
        if (!value || getFieldValue("password") === value) {
          return Promise.resolve();
        }

        return Promise.reject(new Error(VALIDATION_MESSAGE.CONFIRM_PASSWORD));
      },
    }),
  ];
}

const EMAIL_REGEX = new RegExp(
  /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
);

export const isEmail = (email: string): boolean => EMAIL_REGEX.test(email);
