export interface IRoute {
  path: string;
  component: any;
  key: string;
  auth: boolean;
}
