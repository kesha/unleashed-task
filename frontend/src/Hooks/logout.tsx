import { useCallback, useEffect, useRef, useState } from "react";

import { useService } from "./service";
import { useActionCreators } from "./actionCreators";
import { useNavigate } from "react-router-dom";

export const useLogout = () => {
  const ref = useCallback((node: HTMLElement) => {
    setElement(node);
  }, []);
  const [element, setElement] = useState<HTMLElement | null>(null);

  const navigate = useNavigate();
  const { authService } = useService();
  const { logout } = useActionCreators();

  useEffect(() => {
    const node = element;
    if (node) {
      const logoutHandler = () => {
        authService.logout();
        logout();
        navigate("/login");
      };
      node.addEventListener("click", logoutHandler);
      return () => {
        node.removeEventListener("click", logoutHandler);
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [element]);

  return ref;
};
