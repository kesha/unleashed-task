import { LocalStorageKeys } from "@Constants";
import { LocalStorageManager } from "@Helpers";
import { State } from "@Store";
import { useEffect } from "react";
import { useSelector } from "react-redux";

import { useService } from "./service";
import { useActionCreators } from "./actionCreators";

export const useCurrentUser = () => {
  const { me } = useSelector((state: State) => state.user);
  const { loadMe } = useActionCreators();

  const token = LocalStorageManager.get(LocalStorageKeys.AUTH);

  const { profileService } = useService();

  useEffect(() => {
    if (!me && token) {
      profileService.getProfile().then((user) => {
        loadMe(user);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return me;
};
