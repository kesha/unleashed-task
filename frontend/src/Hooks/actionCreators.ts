import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";

import * as actionCreators from "@Store/Actions-Creators";

export const useActionCreators = () => {
  const dispatch = useDispatch();
  return bindActionCreators(actionCreators, dispatch);
};
