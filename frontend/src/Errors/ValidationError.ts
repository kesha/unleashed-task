import BaseError from "./BaseError";

export class ValidationError extends BaseError {
  constructor(message: string, ...codes: string[]) {
    super(message, ...codes);
    this.name = this.constructor.name;
  }
}
