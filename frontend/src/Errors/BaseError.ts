export default class BaseError extends Error {
  codes: string[];
  constructor(message: string, ...codes: string[]) {
    super(message);
    this.codes = codes;
    this.name = this.constructor.name;
  }
}
