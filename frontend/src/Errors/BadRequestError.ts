import BaseError from "./BaseError";

export class BadRequestError extends BaseError {
  constructor(...codes: string[]) {
    super("BAD_REQUEST", ...codes);
    this.name = this.constructor.name;
  }
}
