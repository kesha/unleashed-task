import BaseError from "./BaseError";

export class AssertionError extends BaseError {
  constructor(message: string, ...codes: string[]) {
    super(message, ...codes);
    this.name = this.constructor.name;
  }
}
