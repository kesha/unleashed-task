import BaseError from "./BaseError";

export class UnauthorizedError extends BaseError {
  constructor(...codes: string[]) {
    super("UnauthorizedError", ...codes);
    this.name = this.constructor.name;
  }
}
