import React, { FunctionComponent, memo } from "react";

export const HomePageComponent: FunctionComponent<{}> = memo(() => {
  return <div>Home Page ...</div>;
});
