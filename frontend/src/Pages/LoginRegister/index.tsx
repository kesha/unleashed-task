import { Card, Col, Row } from "antd";
import React, { FunctionComponent, memo, useEffect } from "react";
import { LoginComponent } from "@Components/Login";
import { RegisterComponent } from "@Components/Register";
import { useCurrentUser } from "@Hooks";
import { useNavigate } from "react-router-dom";

export const LoginRegisterPageComponent: FunctionComponent<{}> = memo(() => {
  const user = useCurrentUser();
  const navigate = useNavigate();

  useEffect(() => {
    if (user) {
      navigate("/main");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const style = { padding: "8px" };
  return (
    <>
      <Row>
        <Col
          style={style}
          className="gutter-row"
          xs={24}
          sm={24}
          md={24}
          lg={12}
          xl={12}
        >
          <Card title="Login">
            <LoginComponent />
          </Card>
        </Col>
        <Col
          style={style}
          className="gutter-row"
          xs={24}
          sm={24}
          md={24}
          lg={12}
          xl={12}
        >
          <Card title="Register">
            <RegisterComponent />
          </Card>
        </Col>
      </Row>
    </>
  );
});
